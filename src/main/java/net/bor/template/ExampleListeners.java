package net.bor.template;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author ArtBorax
 */
public class ExampleListeners implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void playerDeath(EntityDeathEvent e) {
        if (!(e instanceof PlayerDeathEvent)) {
            return;
        }
        Player player = (Player) e.getEntity();
        PlayerDeathEvent event = (PlayerDeathEvent) e;
        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Игрок " + player.getName() + " умер");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.isOp()) {
            player.sendMessage(ChatColor.WHITE + "У тебя права оператора, " + ChatColor.YELLOW + "ПОЗДРАВЛЯЮ!");
        }
        if (player.hasPermission("example.admin")) {
            player.sendMessage(ChatColor.RED + "У тебя права администратора, " + ChatColor.YELLOW + "ПОЗДРАВЛЯЮ!");
        }

        player.sendMessage(ChatColor.GREEN + "Привет! Обычный игрок!");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Игрок " + player.getName() + " вышел");
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        //чат
    }

}
